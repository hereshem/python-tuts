from functools import reduce
def main():
    'Lambda functions'
    a = lambda x : x*x
    print(a(5))

    'lambda function with multiple parameter'
    aa = lambda x,y : x+y
    print(aa(4,5))

    'Map function'
    items = [3,5,76,8,9,7]
    m = map(a, items)
    print(set(m))

    for i in m:
        print ("From map",i)

    'Reduce function'
    r = reduce(aa, items)
    print(r)

    'Filter Function'
    def fil(x):
        if x < 7:
            return True
        else:
            return False

    def filt(x):
        return x < 7


    f = filter(lambda x: x < 7, items)
    print(list(f))

    f = filter(lambda x: True if x < 7 else False, items)
    print(list(f))
    f = filter(fil, items)
    print(list(f))
    f = filter(filt, items)
    print(list(f))


main()

