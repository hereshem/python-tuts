def getMin(list):
    if len(list) == 0:
        return None
    min = list[0]
    for item in list:
        if item < min:
            min = item
    return min

def getMax(list):
    if len(list) == 0:
        return None
    min = list[0]
    for item in list:
        if item > min:
            min = item
    return min

def getMinMax(list):
    return getMin(list),getMax(list)

def main():
    'This is main Function'
    list  = [1546,48,590,-7,877,87,456,445,675]
    print ("Minimum value is", getMin(list))
    print("Maximum value is", getMax(list))

    minmax = getMinMax(list)
    print("MinMax value is", minmax)

    min, max = getMinMax(list)
    print("Minimm is ", min, "and maximum is", max)


if __name__ == "__main__":
    main()
