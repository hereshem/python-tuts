
import turtle

t = turtle.Turtle()

def draw_line():
    t.pendown()
    t.forward(50)
    t.penup()
    turtle.mainloop()

def draw_square():
    '''This draws square'''
    t.pendown()
    t.forward(50)
    t.right(90)
    t.forward(50)
    t.right(90)
    t.forward(50)
    t.right(90)
    t.forward(50)
    turtle.mainloop()

def polygon(edges=4, length=50):
    t.pendown()
    angle = 360/edges
    for i in range(edges):
        t.forward(length)
        t.right(angle)
    t.penup()
    turtle.mainloop()

def shapes():
    t.pencolor(0,.5,0)
    for i in range(100):
        t.forward(i)
        t.right(76)
    turtle.mainloop()

def mygra():
    colors = ['red', 'purple', 'blue', 'green', 'yellow', 'orange']
    for x in range(360):
        t.pencolor(colors[x % 6])
        t.width(x / 100 + 1)
        t.forward(x)
        t.left(59)
# polygon()
# polygon(8)
# polygon(8,100)
# shapes()
# mygra()

if __name__ == "__main__":
    draw_line()