
def do_nothing():
    """ Do nothing """


def print_hi():
    print ("Hi")

# do_nothing()
# print_hi()


def printMsg(message):
    print(message)

# printMsg("hello there")


def print_twice(msg):
 printMsg(msg)
 printMsg(msg)

# print_twice("its working!")

name = "Ram"
roll = 26
score = 65.28


def print_user():
    print("User is", name, "with roll", roll, "and score", score)

# print_user()

counter = 0


def increase_count():
    global counter
    counter += 1
    if counter == 0 :
        print("Counter is", counter)
    else:
        print("Counter is not 0 but", counter)

# increase_count()
# increase_count()
# increase_count()

def print_box(msg, pattern="*"):
    print(pattern * (len(msg) + 4))
    print(pattern, msg, pattern)
    print(pattern * (len(msg) + 4))

def get_input_and_show():
    msg = input("Enter string : ")
    pattern = input("Enter pattern : ")
    if(len(pattern) > 0):
        print_box(msg, pattern)
    else:
        print_box(msg)


def print_tree(msg):
    '''This prints tree structure for given string'''
    for i in range(len(msg)):
        print(" "*int((len(msg)-i)/2),msg[:i])


def rev_string(str):
    ''

def main():
    ''
    print_tree("Hello there")

if __name__ == "__main__":
    main()
# print_tree("BroadWay Infosis")
# get_input_and_show()
# print_box("Hello this is python class", "#")
