import time
my_dict = dict()
def factorial(num):
    if num == 0 :
        return 1
    else:
        fact = num * factorial(num - 1)
        my_dict[num] = fact
        return fact
# 0000.00007796 second - 77 micro
# 00.000003814697265625 - 3 micro
def get_input():
    while True:
        num = int(input("Enter number : "))
        start = time.time()
        if num == 0:
            break
        elif num in my_dict:
            fact = my_dict[num]
            end = float(time.time())
            print ("Factorial is", fact, "with execution time", end, start, end - start)
        else:
            fact = factorial(num)
            my_dict[num] = fact
            end = time.time()
            print("Calculated factorial is", fact, "with execution time", end - start, my_dict)

def sum(a,b):
    return a+b

def sumall(*params):
    ''

def printall(*param): # reducing feature of tuple
    print(param)

def main():
    #get_input()
    s = sum(4,5)
    print (s)

    t = (4,5)
    s = sum(*t) # expanding feature of tuple
    print(s)

    printall(2,"number 2 and 3", 3)

    s = sumall(1,2,43,5,5,6)
    print (s)


if __name__ == "__main__":
    main()