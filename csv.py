



def read_file():
    '''Returns record of each student in List form
    [
    [1,"name1",45,5,6,],
    [2,"sffs",34,34,34,4]
    ]
    '''
    with open("student.csv") as f:
        keys = []
        records = []
        for line in f:
            record = line.strip().split(",")
            if(len(keys) == 0):
                keys = record
            # print(record)
            else:
                records.append(record)
        # print(keys)
    return records

def read_file_as_dict():
    f = open("flooorsheet.csv")
    keys = []
    records = []
    for line in f:
        record = line.strip().split(",")
        if len(keys) == 0:
            keys = record
        else:
            d = dict(zip(keys, record))
            records.append(d)
    return records

def calculate_marks():
    data = read_file()
    for st in data:
        total = 0
        fail = False     #
        for i in range(3,8):
            num = int(st[i])#
            if(num < 32):   #
                fail = True #
            total += num
        print("Total marks for", st[1], "is", total,
              "with result", "Fail" if fail else "Pass")

#calculate_marks()
if __name__ == "__main__":
    calculate_marks()



# print(read_file())
