import datetime
import pytz

def main():
    d = datetime.datetime.now()
    'Set d as current time'

    print("Current time is", d)

    'Multiple timezone specific times'
    zones = ["Asia/Kathmandu", "GMT", "Japan", "Singapore"]
    tzs = list(map(lambda x:pytz.timezone(x), zones))

    for zone,tz in zip(zones, tzs):
        print("Time in", zone, "is", d.astimezone(tz))

    'Timestamps'
    print("Curent timestamp is", d.timestamp())
    print("Time from timestamp 0 is", d.fromtimestamp(0))
    print("Time from timestamp 0 for UTC is", d.fromtimestamp(0, tz=pytz.utc))

    'Time Formating'
    print("Time formatted data is", d.strftime("%Y-%m-%d %H:%M:%S"))
    print("Time formatted data for Japan is", d.astimezone(pytz.timezone("Japan")).strftime("%Y-%m-%d %H:%M:%S"))

    'String to time object'
    print("Date object from String is", d.strptime("2018-05-04 13:45:35","%Y-%m-%d %H:%M:%S"))
    print("Date object from String is", d.strptime("2018-05-04 13:45:35 +0545", "%Y-%m-%d %H:%M:%S %z"))

    'converting from one timezone string to another'
    print("Date object from String is", d.strptime("2018-05-04 13:45:35 +0545","%Y-%m-%d %H:%M:%S %z").astimezone(pytz.timezone("Japan")).strftime("%Y-%m-%d %H:%M:%S %z"))
    print("Date object initialization", datetime.datetime(2018,5,4,13,45,35))

    'Time Delta'
    print("My age is", d-datetime.datetime(2000,1,1))
    print("5 days from now is" , d + datetime.timedelta(days=5))
    print("5 weeks before than current is", d - datetime.timedelta(weeks=5))

if __name__ == "__main__":
    main()