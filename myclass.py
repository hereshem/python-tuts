class Website:
    'Stores website information'
    #pass
    def __init__(self, url, name="", is_online=True):
        'Constructor'
        self.name = name
        self.url = url
        self.is_online = is_online

    def __str__(self):
        return "Website info for " + str(self.__dict__)

    def show_name(self):
        print(self.name)

    def get_name(self):
        return self.name

    def set_name(s, name):
        s.name = name

def main():
    w = Website("https://github.com", "Github Site", True)
    print(w)
    # w.url = "github.com"
    # # w.name = "Github Website"
    # w._is_online = True
    # w.__parent = "Microsoft"
    print(w)
    w.show_name()
    w.set_name("GitHub")
    w.show_name()

    g = Website("https://google.com", "Google", False)
    print(g)

    h = Website("http://example.com")
    print(h)

    i = Website(True)
    print(i.__dict__)

    import my_dict
    a = my_dict.countChar("hello there")
    print(a)

if __name__ == "__main__":
    main()