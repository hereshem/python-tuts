list = []

def show_options():
    options = ["Option 0 : Exit",
               "Option 1 : Check your name",
               "Option 2 : Enter your name",
               "Option 3 : Remove your name",
               "Option 4 : List all names"
            ]
    opt = input ("\n".join(options))
    return opt

def check_opt(opt):
    if opt == "0":
        'exit from loop'

    elif opt == "1":
        'check your name'
        name = input("Enter your name to check")
        if name in list:
            print ("I know your name")
        else:
            print ("I don't know your name")

    elif opt == "2":
        'enter your name'
        name = input("Enter your name for entry")
        list.append(name)

    elif opt == "3":
        'remove your name'
        name = input ("Enter name to remove")
        if name in list:
            list.remove(name)
            print ("Your name is removed")
        else:
            print("You were not in list")

    elif opt == "4":
        'show names list'
        for name in list:
            print(name)

def main():
    while True:
        opt = show_options()
        if opt == 0:
            break
        check_opt(opt)


if __name__ == "__main__":
    main()

