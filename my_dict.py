def countChar(mystring):
    """
    Counts characters in the string using dictionary
    :return:
    """
    dic = {}
    for ch in mystring:
        if ch not in dic: #dic[ch] == None:
            dic[ch] = 1
        else:
            dic[ch] += 1
    return dic


def main():
    'Main function'
    dic = countChar("This is classroom for python")
    for k in dic:
        print(k, dic[k])

    for k in dic.items():
        print (k)

    for k,v in dic.items():
        print (k,v)

    print (dic)

if __name__ == "__main__":
    main()